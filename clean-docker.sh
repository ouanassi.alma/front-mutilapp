#!/bin/bash

echo "Cleaning docker"
docker container stop $(docker container ls -aq)
docker container rm $(docker container ls -aq)
# docker system prune -a
docker rmi $(docker images -a -q)
# docker volume rm $(docker volume ls -aq)
# Remove unused volumes using "rm" or "prune".
docker volume rm -f $(docker volume ls -f)
docker volume prune -f

# Remove unused networks.
docker network rm -f $(docker network ls -f)
docker network prune -f

echo "Cleaning successfull"

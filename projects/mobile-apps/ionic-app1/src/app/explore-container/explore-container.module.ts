import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponent } from './explore-container.component';
import { SharedComponentsModule } from 'projects/shared-code/components/shared-components.module';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule, SharedComponentsModule],
  declarations: [ExploreContainerComponent],
  exports: [ExploreContainerComponent]
})
export class ExploreContainerComponentModule {}
